#include <eosio/eosio.hpp>

using namespace eosio;

class [[eosio::contract]] hello : public contract {
    
    public:
        using contract::contract;
        
        [[eosio::action]]
              
	    void greet(name _first, ) {
            	require_auth( _first );
		print("Hello, ", _first );
            }
};







